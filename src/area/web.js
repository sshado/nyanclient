import Timeline from '@totemTimeline/index.js';

export default {
    webServer: Timeline.webQuery("serverSelected", args => args.serverUrl, (options) => {
        console.log("Web Query [Selected Server] returned.");
        return {
            headers: {
                //"Authorization": `Bearer token`,
                "Connection": `keep-alive`
            }
        }
    })
}