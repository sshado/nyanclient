import Timeline from "@totemTimeline/index.js";

Timeline.topic({
    name: "ServerSelector",
    when: {
        selectServer(e) {
            console.log("Topic [Server Selector] When [Select Server]: A server is being selected.");
            this.then("serverSelected", {
                serverUrl: `/api/serverurl/1`,
                server: `Web Query Server`,
                name: `Test Name`
            })
        }
    }
});