import Timeline from "@totemTimeline/index.js";

export default {
  universe: Timeline.query({
    name: "Universe",
    given: {
      serverSelected(e){
        this.server = e.name;
        this.loading = false;
      }
    }        
  })
};